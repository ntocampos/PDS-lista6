n = 0:99;
fs = 200;
Ts = 1/fs;

x = cos(0.48*pi*n) + cos(0.52*pi*n);
X = fft(x);

m = 0:length(X) - 1;

subplot(3, 1, 1);
stem(x);
xlabel('n');
ylabel('x(n)');
title('Sequencia');

subplot(3, 1, 2);
stem(m*fs/length(X), abs(X), 'b');
xlabel('Frequencia em Hz');
ylabel('Magnitude');
title('Magnitude da Resposta em Frequencia');

subplot(3, 1, 3);
stem(m*fs/length(X), angle(X), 'b');
xlabel('Frequencia em Hz');
ylabel('Angulo');
title('Fase');